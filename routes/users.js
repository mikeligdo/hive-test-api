const express = require('express')
const router = express.Router()
const { getAllUsers, getUser,  updateUser, deleteUser } = require('../controllers/users')

router.get('/AllUsers', getAllUsers)
router.get('/:id', getUser)
router.patch('/:id', updateUser)
router.delete('/:id', deleteUser)


module.exports = router