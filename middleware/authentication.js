// const User = require("../models/User")
const jwt = require('jsonwebtoken')
const CustomAPIError = require('../errors/custom-error')

const auth = async (req, res, next) => {
    const authHeader = req.headers.authorization
    if(!authHeader || !authHeader.startsWith('Bearer')){
        throw new CustomAPIError('Authentication invalid', 511)
    }   
    const token = authHeader.split(' ')[1]
    try {
        const payload = jwt.verify(token, process.env.JWT_SECRET)
        console.log(payload);
        req.user = { userId:payload.userId }
        next()
    } catch (error) {
        throw new CustomAPIError('Authentication invalid', 511)
    }
}

module.exports = auth