const mongoose = require('mongoose')
const bycrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const UserSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: [true, 'Please provide First Name'],
        minlength: 3,
        maxlength: 50
    },
    lastname: {
        type: String,
        required: [true, 'Please provide Last Name'],
        minlength: 3,
        maxlength: 50
    },
    email: {
        type: String,
        required: [true, 'Please provide email'],
        match: [
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            'Please provide a valid email',
        ],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'Please provide password'],
        minlength: 6
    },
    role: {
        type: String,
        default: 'User'
    },
    contacts: {
        type: Array,
    },
    request_in: {
        type: Array,
    },
    request_out: {
        type: Array,
    },

},
    { timestamps: true }
)

UserSchema.pre('save', async function() {
    this.password = await bycrypt.hash(this.password, 10)
})

UserSchema.methods.createJWT = function() {
    return jwt.sign({userId: this._id}, process.env.JWT_SECRET, { expiresIn: process.env.JWT_LIFETIME })
}


UserSchema.methods.comparePassword = async function(userPassword) {
    return await bycrypt.compare(userPassword, this.password)
}

module.exports = mongoose.model('User', UserSchema)