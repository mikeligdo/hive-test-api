const User = require('../models/User')
const CustomAPIError = require('../errors/custom-error')

const getAllUsers = async (req, res) => {
    const jobs = await User.find({}).sort('createdAt')
    res.status(200).json(jobs)
}
const getUser = async (req, res) => {
    const { params:{ id:jobId } } = req
    const user = await User.findOne({  _id: jobId })
    if (!user) {
        throw new CustomAPIError('No User Found', 404)
    }
    res.status(200).json(user)
}
const updateUser = async (req, res) => {
    const { body:{ firstname, lastname, role }, params:{ id:userId } } = req

    if (firstname === '' || lastname === '' || role === '') {
        throw new CustomAPIError('Please Fill All the data', 404)
    }

    const user = await User.findOneAndUpdate({ _id:userId }, req.body, {new: true, runValidators: true})

    if (!user) {
        throw new CustomAPIError('No User Found', 404)
    }

    res.status(200).json(user)

}
const deleteUser = async (req, res) => {
    const { params:{ id:userId } } = req

    const user = await User.findByIdAndRemove({_id: userId})

    if (!user) {
        throw new CustomAPIError('No User Found', 404)
    }

    res.status(200).send('User Deleted')
}

module.exports = { getAllUsers, getUser, updateUser, deleteUser }