const User = require('../models/User')
const CustomAPIError = require('../errors/custom-error')


const register = async (req, res) => {
    const user = await User.create({...req.body})

    const token = user.createJWT()
    res.status(201).json({token})

    if (res.status != 201) {
        console.log(err);
    }
}

const login = async (req, res) => {
    const { email, password } = req.body

    if (!email || !password) throw new CustomAPIError('Please provide all the details', 400)
    
    const user = await User.findOne({email})

    if (!user) throw new CustomAPIError('Invalid Credentials', 401)

    const isPasswordCorrect = await user.comparePassword(password)

    if(!isPasswordCorrect) throw new CustomAPIError('Invalid Credentials', 401)

    const token = user.createJWT()
    // console.log(user);
    res.status(200).json({userId:user._id, token }) 
}

module.exports = { register, login}